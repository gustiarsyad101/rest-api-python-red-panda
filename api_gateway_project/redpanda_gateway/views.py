from django.shortcuts import render

import requests
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# library
from django.http import JsonResponse
from . models import MessageModel

import pymysql

# Create your views here.
# Post data to Red Panda
@csrf_exempt
def redpanda_messages(request):
    if request.method == 'POST':
        try:
            # Forward the POST request to the Redpanda API
            redpanda_api_endpoint = 'https://redpanda.com/api/messages' # get url from red panda
            response = requests.post(redpanda_api_endpoint, data=request.POST)
            
            # Return the response from the Redpanda API back to the client
            return JsonResponse(response.json(), status=response.status_code)
        except requests.exceptions.RequestException as e:
            return JsonResponse({'error': str(e)}, status=500)
    else:
        return JsonResponse({'error': 'Invalid request method'}, status=405)
        
# Get data from Red Panda
# app_name/views.py
def get_data(request):
    data = MessageModel.objects.all().values()
    return JsonResponse(list(data), safe=False)

def fetch_data_from_api():
    url = "http://127.0.0.1:8000/get_data/"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        return data
    else:
        return None

def save_data_to_mariadb(data):
    # Replace with your MariaDB connection details
    connection = pymysql.connect(
        host="http://127.0.0.1:8000/",
        user="root",
        password="Y123Kaca#",
        database="your-mariadb-database"
    )

    try:
        with connection.cursor() as cursor:
            # Replace with the appropriate SQL query to save the data to MariaDB
            sql = "INSERT INTO message_red_panda (messageStringData, messageNumberData) VALUES (%s, %s)"
            for item in data:
                cursor.execute(sql, (item['messageStringData'], item['messageNumberData']))
        connection.commit()
    finally:
        connection.close()
