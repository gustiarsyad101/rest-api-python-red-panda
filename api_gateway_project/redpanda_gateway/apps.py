from django.apps import AppConfig


class RedpandaGatewayConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'redpanda_gateway'
