from django.db import models

# Create your models here.
# app_name/models.py
from django.db import models

class MessageModel(models.Model):
    # Define your model fields here
    messageStringData = models.CharField(max_length=100)
    messageNumberData = models.IntegerField()
    # Add more fields as needed

