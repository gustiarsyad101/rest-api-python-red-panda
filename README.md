app22-IdeaPad-3-14ITL6% python3 -m venv django-env
app22-IdeaPad-3-14ITL6% source django-env/bin/activate
(django-env) app22-IdeaPad-3-14ITL6% python -m pip install django
Collecting django
  Using cached Django-4.2.3-py3-none-any.whl (8.0 MB)
Collecting asgiref<4,>=3.6.0
  Using cached asgiref-3.7.2-py3-none-any.whl (24 kB)
Collecting sqlparse>=0.3.1
  Using cached sqlparse-0.4.4-py3-none-any.whl (41 kB)
Collecting typing-extensions>=4
  Using cached typing_extensions-4.7.1-py3-none-any.whl (33 kB)
Installing collected packages: typing-extensions, sqlparse, asgiref, django
Successfully installed asgiref-3.7.2 django-4.2.3 sqlparse-0.4.4 typing-extensions-4.7.1
(django-env) app22-IdeaPad-3-14ITL6% django-admin startproject api_gateway_project
(django-env) app22-IdeaPad-3-14ITL6% cd api_gateway_project
(django-env) app22-IdeaPad-3-14ITL6% python manage.py startapp redpanda_gateway
                                                                                                                                                                                                                                                       
(django-env) app22-IdeaPad-3-14ITL6% pip list
Package           Version
----------------- -------
asgiref           3.7.2
Django            4.2.3
pip               22.0.2
setuptools        59.6.0
sqlparse          0.4.4
typing_extensions 4.7.1
(django-env) app22-IdeaPad-3-14ITL6% sudo pip install requests 
Requirement already satisfied: requests in /usr/lib/python3/dist-packages (2.25.1)
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
(django-env) app22-IdeaPad-3-14ITL6% sudo pip3 install requests
Requirement already satisfied: requests in /usr/lib/python3/dist-packages (2.25.1)
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
(django-env) app22-IdeaPad-3-14ITL6% pip3 install requests 
Collecting requests
  Using cached requests-2.31.0-py3-none-any.whl (62 kB)
Collecting charset-normalizer<4,>=2
  Downloading charset_normalizer-3.2.0-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (201 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 201.8/201.8 KB 2.3 MB/s eta 0:00:00
Collecting idna<4,>=2.5
  Using cached idna-3.4-py3-none-any.whl (61 kB)
Collecting certifi>=2017.4.17
  Downloading certifi-2023.7.22-py3-none-any.whl (158 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 158.3/158.3 KB 27.3 MB/s eta 0:00:00
Collecting urllib3<3,>=1.21.1
  Downloading urllib3-2.0.4-py3-none-any.whl (123 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 123.9/123.9 KB 20.3 MB/s eta 0:00:00
Installing collected packages: urllib3, idna, charset-normalizer, certifi, requests
Successfully installed certifi-2023.7.22 charset-normalizer-3.2.0 idna-3.4 requests-2.31.0 urllib3-2.0.4

(django-env) app22-IdeaPad-3-14ITL6% pip3 install pymysql
Collecting pymysql
  Using cached PyMySQL-1.1.0-py3-none-any.whl (44 kB)
Installing collected packages: pymysql
Successfully installed pymysql-1.1.0
(django-env) app22-IdeaPad-3-14ITL6% )

# How to run the apps
(django-env) app22-IdeaPad-3-14ITL6% python manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
July 27, 2023 - 04:22:47
Django version 4.2.3, using settings 'api_gateway_project.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.

Not Found: /
[27/Jul/2023 04:22:52] "GET / HTTP/1.1" 404 2105
Not Found: /favicon.ico
[27/Jul/2023 04:22:52] "GET /favicon.ico HTTP/1.1" 404 2156

# Run rest api get data from Red Panda
(django-env) app22-IdeaPad-3-14ITL6% python manage.py makemigrations
python manage.py migrate
No changes detected
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying sessions.0001_initial... OK
(django-env) app22-IdeaPad-3-14ITL6% 

# set database of MariaDB
1. Check of status mariaDB
app22-IdeaPad-3-14ITL6% sudo systemctl status mariadb
[sudo] password for app22: 
● mariadb.service - MariaDB 10.6.12 database server
     Loaded: loaded (/lib/systemd/system/mariadb.service; enabled; vendor prese>
     Active: active (running) since Thu 2023-07-27 09:44:52 WIB; 3h 41min ago
       Docs: man:mariadbd(8)
             https://mariadb.com/kb/en/library/systemd/
    Process: 857 ExecStartPre=/usr/bin/install -m 755 -o mysql -g root -d /var/>
    Process: 895 ExecStartPre=/bin/sh -c systemctl unset-environment _WSREP_STA>
    Process: 904 ExecStartPre=/bin/sh -c [ ! -e /usr/bin/galera_recovery ] && V>
    Process: 1181 ExecStartPost=/bin/sh -c systemctl unset-environment _WSREP_S>
    Process: 1188 ExecStartPost=/etc/mysql/debian-start (code=exited, status=0/>
   Main PID: 944 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 8 (limit: 23660)
     Memory: 123.8M
        CPU: 3.635s
     CGroup: /system.slice/mariadb.service
             └─944 /usr/sbin/mariadbd

Jul 27 09:44:52 app22-IdeaPad-3-14ITL6 systemd[1]: Started MariaDB 10.6.12 data>
Jul 27 09:44:52 app22-IdeaPad-3-14ITL6 /etc/mysql/debian-start[1192]: Upgrading>
Jul 27 09:44:52 app22-IdeaPad-3-14ITL6 /etc/mysql/debian-start[1195]: Looking f>
Jul 27 09:44:52 app22-IdeaPad-3-14ITL6 /etc/mysql/debian-start[1195]: Looking f>
Jul 27 09:44:52 app22-IdeaPad-3-14ITL6 /etc/mysql/debian-start[1195]: This inst>
lines 1-23

Example: 
MYSQL   : root
pwd: db : Y123Kaca#

